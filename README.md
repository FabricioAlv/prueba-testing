# Prueba Testing

## Indice
* [Roles](#roles)
    * [Guest](#guest)
    * [Reporter](#reporter)
    * [Developer](#developer)
    * [Maintainer](#maintainer)


## Roles

### *Guest* 
![Guest](https://cdn3.iconfinder.com/data/icons/hotel-114/96/Account_guest_hotel_user-512.png)

- Tiene permitido la descarga del proyecto por tan solo una ocación.
- En este rol se puede crear issues, pero simplemente uno.
- No tiene permitido la creación de ramas.
- No permite crear labels, solamente el creador del proyecto lo puede editar.
- No se permite la creación de milestones.
- No se permite realizar commits, push.
- Es posible realizar unicamente pull con este usuario, solo una vez.
- No es posible eliminar las ramas creadas.


### *Reporter*
![Reporter](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSYSKyt7nUk89lje8UcMXbItI0wEycXeB0AV9W1AVN0JWdruhKY)

- Tiene el privilegio de descarga del proyecto.
- En este rol se puede crear issues .
- No tiene permitido la creaciòn de ramas.
- Permite crear labels, weight, asignación
- No se permite la creación de milestones.
- No se permite realizar commits, push.
- Es posible realizar unicamente pull con este usuario
- No es posible eliminar las ramas creadas.


### *Developer*
![Developert](https://cdn2.iconfinder.com/data/icons/computer-63/100/Dev-05-512.png)

- Tiene el privilegio de descarga del proyecto.
- Es posible la creación de issues.
- Permite crear labels, weight, asignación
- Este usuario tiene la posibilidad de crear/editar7borrar milestones.
- Se puede realizar la acción de pull con este usuario.
- Este usuario tiene la opción de crear ramas.
- Es posible crear Merge request
- El usuario tiene la posibilidad de realizar commit y push en el proyecto.
- Se puede eliminar cualquier rama siempre y cuando no sea la rama master.


### *Maintainer*
![Guest](https://www.gsimsl.com/uploads/1/7/0/6/17060032/6578114_orig.jpg)

- Tiene el privilegio de descarga del proyecto.
- Es posible la creación de issues.
- Permite crear labels, weight, asignación.
- Este usuario tiene la posibilidad de crear/editar7borrar milestones.
- Se puede realizar la acción de pull con este usuario.
- Este usuario tiene la opción de crear ramas.
- Es posible crear Merge request.
- Se puede eliminar cualquier rama siempre y cuando no sea la rama master.
- El usuario tiene la posibilidad de realizar commit y push en el proyecto.
- Posee el privilegio de añadir nuevos colaboradores al proyecto.

